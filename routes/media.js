const express = require('express');
const router = express.Router();
const isBase64 = require('is-base64')
const base64Img = require('base64-img')
const fs = require('fs')

const { Media } = require('../models')

// create get image
router.get('/', async(req, res) => {
  //findAll for get all data from Media
  const media = await Media.findAll({
    // attributes just choice data what you need ex: only id & image
    attributes: ['id', 'image']
  });

  const mappedMedia = media.map((m) => {
    m.image = `${req.get('host')}/${m.image}`
    return m
  })
  return res.json({
    status: 'success',
    data: mappedMedia
  })
})

// create post/upload image
router.post('/', (req, res) => {
  const image = req.body.image

  // mimerequired is data:image/png
  if(!isBase64(image, { mimeRequired: true })) {
    return res.status(400).json({status: 'error', message: 'invalid base64'})
  }
  base64Img.img(image, './public/images', Date.now(), async (err, filepath) => {
    if(err) {
      return res.status(400).json({status: 'error', message: err.message})
    }

    // filepath is location image which already upload
    // example : public/images/homeimages.png
    const filename = filepath.split('/').pop() //get the last array ex: /homeimages.png
    
    const media = await Media.create({ image: `images/${filename}` }) //upload image to file public/images
    return res.json({
      status: 'success',
      data: {
        id: media.id,
        image: `${req.get('host')}/images/${filename}`
      }
    })
  })
})

// delete image
router.delete('/:id', async (req, res) => {
  const id = req.params.id
  
  const media = await Media.findByPk(id) //findByPk is findby primary key
  if(!media) {
    return res.status(404).json({ status: 'error', message: 'media not found'})
  }

  // fs.unlink is method for delete file with callback
  fs.unlink(`./public/${media.image}`, async(err) => {
    if(err) {
      return res.status(400).json({ status: 'error', message: err.message })
    }

    await media.destroy()

    return res.json({
      status: 'success',
      message: 'image deleted'
    })
  })
})

module.exports = router;
