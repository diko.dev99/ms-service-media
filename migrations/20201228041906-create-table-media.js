'use strict';

module.exports = {
  // up is, when you run command sequalize db.migrate
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('media', {
      id: {
        type: Sequelize.INTEGER, //cause make this primary key
        primaryKey: true,
        autoIncrement: true,
        allowNull: false, //can null ? if false can't null
      },
      image: {
        type: Sequelize.STRING,
        allowNull: false
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false
      },
    });
  },
  
  // if you run command sequalize db.migrate:undo
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('media');
  }
};
